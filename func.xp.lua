--[[
	BarMath 2.x
	Experience Bar Functions
	Version 1.0
]]

BarMath_PetXP_Version = "1.0";			-- Addon Version. Should always be "BarMath_(Addon Title)_Version".
BarMath_PetXP_DBRewrite_Version = "1.0";	-- States the version of the addon that should totally rewrite the options.

local BMPXP_Prediciton = "Mob";
local BMPXP_CurrentXP = GetPetExperience();
local BMPXP_LastXP = BMPXP_CurrentXP;

-- These XP Defaults are the minimum default requirements for a BarMath Bar! These MUST be listed first!
BarMath_Defaults_PetXP = {
	["Version"] = BarMath_PetXP_Version,	-- Required Variable as of BarMath 2.0.4
	["Display"] = 1,			-- 1 = On, 0 = Off
	["DisplayMethod"] = 1,			-- 1 = Text is Always on, 0 = Text only when mouseover
	["TextLength"] = BARMATH_TEXT_SHORT,	-- Other Value: BARMATH_TEXT_LONG
	["Displays"] = {
		["Display1"] = 1,		-- Determines the default option for the Displays.
		["Display2"] = 2,
		["Display3"] = 3,
		["Display4"] = 4,
		["Display5"] = 5,
		["Display1OnOff"] = 1,		-- Default setting for if this Display is visible. 1 = Visible, 0 = Hidden.
		["Display2OnOff"] = 1,
		["Display3OnOff"] = 1,
		["Display4OnOff"] = 1,
		["Display5OnOff"] = 1,					
	},
	["PredictionTrackKills"] = 30,	-- The Number of Mob Kills to Average for the Prediciton of how many Mobs to Level
	["Prediction_MobList"] = {},
}

-- Add Bar to Barlist
	-- First Variable is the Bar's Name. With our XP example, the final name for the bar will be "BarMathPetXPBar".
	-- Second is the update function. This is called to update the bar's data.
	-- Third is the default option settings for this bar.
	-- Fourth variable is the parent. If not set, or the bar doesn't exist, then BarMath will automatically assign a parent.
	-- Fifth variable is the InfoDelete function that should be called when deleting a character's information from the Info Database.
	-- Sixth is the template this bar should follow. This is useful for if you want to make your own bar template, say with 4 bars instead of 1.
if (BarMath_EngClass == "HUNTER") then
	BarMath_AddBar("PetXP", "BarMath_PetXP_Update", BarMath_Defaults_PetXP);
end

-- Option Text
-- The following is Option Text. All of them MUST be in an identical order!

-- Dropdown Text
BarMath_Option_DropDown_PetXP_Listing = {
	"BARMATH_DISPLAYOPTION_BAR_FILLED", 		-- Amount of Bars Filled
	"BARMATH_DISPLAYOPTION_BAR_PERBAR",		-- Amount Per Bar
	"BARMATH_DISPLAYOPTION_BAR_CURRMAX",		-- The Current / The Maximum
	"BARMATH_DISPLAYOPTION_BAR_TOLVL",		-- Amount needed to Level (Max - Current)
	"BARMATH_DISPLAYOPTION_BAR_TOFILL",		-- Bars needed to Level
	"BARMATH_DISPLAYOPTION_PERC_FILLED",		-- Percent Filled
	"BARMATH_DISPLAYOPTION_PERC_TOFILL",		-- Percent Needed to Level
	"BARMATH_DISPLAYOPTION_PERC_PERBAR",		-- XP Per Percent
	"BARMATH_DISPLAYOPTION_PXPTOMAX",		-- XP needed for Level 80
	"BARMATH_DISPLAYOPTION_BARSTOMAX",		-- Bars needed for Level 80
	"BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVEL",
	"BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVELWAVG",
};

-- Long Text
BARMATH_TXT_PXP_LONG = {
	BARMATH_BARTXT_PEXP_BARS_FILLED,		-- BARMATH_DISPLAYOPTION_BAR_FILLED
	BARMATH_BARTXT_PEXP_PERBAR,			-- BARMATH_DISPLAYOPTION_BAR_PERBAR
	BARMATH_BARTXT_PEXP, 				-- BARMATH_DISPLAYOPTION_BAR_CURRMAX
	BARMATH_BARTXT_PEXP_NEEDED_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_BAR_TOLVL
	BARMATH_BARTXT_PEXP_BARS_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_BAR_TOFILL
	BARMATH_BARTXT_PEXP_PERCENT_EARNED,		-- BARMATH_DISPLAYOPTION_PERC_FILLED
	BARMATH_BARTXT_PEXP_PERCENT_TO_LEVEL,		-- BARMATH_DISPLAYOPTION_PERC_TOFILL
	BARMATH_BARTXT_PEXP_PER_PERCENT,		-- BARMATH_DISPLAYOPTION_PERC_PERBAR
	BARMATH_BARTXT_PEXP_TO_MAX,			-- BARMATH_DISPLAYOPTION_PetXPTOMAX
	BARMATH_BARTXT_PEXP_BARS_TO_MAX,		-- BARMATH_DISPLAYOPTION_BARSTOMAX
	BARMATH_BARTXT_PEXP_PREDICTION_MOBSTOLEVEL,
	BARMATH_BARTXT_PEXP_PREDICTION_AVERAGE,
}

-- Short Text
BARMATH_TXT_PXP_SHORT = {
	BARMATH_BARTXT_PXP_BARS_FILLED,
	BARMATH_BARTXT_PXP_PERBAR,
	BARMATH_BARTXT_PXP,
	BARMATH_BARTXT_PXP_NEEDED_TO_LEVEL,
	BARMATH_BARTXT_PXP_BARS_TO_LEVEL,
	BARMATH_BARTXT_PXP_PERCENT_EARNED,
	BARMATH_BARTXT_PXP_PERCENT_TO_LEVEL,
	BARMATH_BARTXT_PXP_PER_PERCENT,
	BARMATH_BARTXT_PXP_TO_MAX,
	BARMATH_BARTXT_PXP_BARS_TO_MAX,
	BARMATH_BARTXT_PXP_PREDICTION_MOBSTOLEVEL,
	BARMATH_BARTXT_PXP_PREDICTION_AVERAGE,
}


-- Get XP amount for current Level
function BarMath_GetLVLXP(level)
	local diff, xp, rf, mxp = 0;

	-- MXP figure out
	if (level < 60) then
		mxp = 45+(5*lvl);
	else
		mxp = 235+(5*lvl);
	end
	
	diff[29] = 1;
	diff[30] = 3;
	diff[31] = 6;
	for c=32,59,1 do
		diff[c] = 5*(level - 30);
	end
	if not diff[level] then
		diff[level] = 0;
	end

	-- RF
	rf = 1;
	if (level <= 27) then
		rf = (1-(level-10)/100);
	elseif (level <= 59) then
		rf = 0.82;
	end

	-- Final calculation
	xp = BarMath_Round((((8 * level)+diff[level]) * mxp * rf)/100,BARMATH_ROUNDING_DOWN,0);
	return xp;
end

-- XP Table
BarMath_PetXP_Table = {
	[10] = 760,
	[11] = 870,
	[12] = 980,
	[13] = 1100,
	[14] = 1230,
 	[15] = 1360,
 	[16] = 1500,
 	[17] = 1640,
 	[18] = 1780,
	[19] = 1930,
 	[20] = 2080,
	[21] = 2240,
 	[22] = 2400,
	[23] = 2550,
 	[24] = 2720,
 	[25] = 2890,
 	[26] = 3050,
 	[27] = 3220,
 	[28] = 3390,
 	[29] = 3630,
 	[30] = 3880,
 	[31] = 4160,
 	[32] = 4460,
	[33] = 4800,
 	[34] = 5140,
 	[35] = 5500,
 	[36] = 5870,
 	[37] = 6240,
 	[38] = 6620,
 	[39] = 7020,
 	[40] = 7430,
 	[41] = 7850,
 	[42] = 8280,
	[43] = 8710,
 	[44] = 9160,
 	[45] = 9530,
 	[46] = 10100,
 	[47] = 10580,
 	[48] = 11070,
 	[49] = 11570,
 	[50] = 12090,
 	[51] = 12610,
 	[52] = 13150,
	[53] = 13700,
 	[54] = 14250,
 	[55] = 14820,
 	[56] = 15400,
 	[57] = 15990,
 	[58] = 16580,
 	[59] = 17200,
 	[60] = 29000,
 	[61] = 31700,
 	[62] = 34900,
	[63] = 38600,
 	[64] = 42800,
 	[65] = 47500,
 	[66] = 52700,
 	[67] = 58500,
 	[68] = 64800,
 	[69] = 71700,
	[70] = 152380,
	[71] = 153960,
	[72] = 155570,
	[73] = 157180,
	[74] = 158790,
	[75] = 160420,
	[76] = 162070,
	[77] = 163740,
	[78] = 165390,
	[79] = 167080,
}

-- XP Update function. This is the master function that should be called when a change is made to the XP Bar.
-- All XP functions should only be used in here to reduce memory usage.
function BarMath_PetXP_Update()
	if (BarMath_HaveLoaded["Bars"]["PetXP"]) then
		-- Call function to see how many times this is ran. Should be deleted or commented out before release!
		-- BarMath_Msg("Pet XP Bar Loaded...");

		local hasUI, isHunterPet = HasPetUI();

		if (not hasUI) or (not isHunterPet) then
			if (BarMathPetXPBarDisplay3Text == nil) then
				BarMath_SetBarText(BARMATH_NOPETDATA, "PetXP", 3);
			end
			return;
		end

		if (UnitLevel("pet") == UnitLevel("player")) then
			BarMathPetXPBar:Hide();
			BarMath_Post_Load();
			return;
		elseif (not BarMathPetXPBar:IsVisible()) then
			BarMathPetXPBar:Show();
			BarMath_Post_Load();
		end

		-- Set Locals
		local post, P_mobs, P_M_avg;
		local option, display = {}, {};
		local currXP, nextXP = GetPetExperience();

		local Pmobs = BarMath_GetVar("Bars","PetXPBar","Prediction_MobList");
		
		if Pmobs == nil or #Pmobs == 0 then
			Pmobs = {};
		end

		if BMPXP_LastXP ~= currXP then
			BMPXP_LastXP = BMPXP_CurrentXP;
		end
		BMPXP_CurrentXP = currXP;

		-- Run Prediction Function
		BarMath_PetXP_Prediciton();

		-- Figure out everything!
		local perbar, bars, togo, XPtogo, pcent, pcent_togo = BarMath_PetXP_Calculate();
		local xpperpcent = BarMath_Round(nextXP/100);

		local remaining = nextXP - currXP;
		local xptomax = BarMath_PetXPtoMax();
		local barstomax = ((BarMath_MaxLVL-UnitLevel("player"))*BarMath_NumBars)-bars;
		if #Pmobs == 0 then
			P_mobs = "Unknown";
			P_M_avg = "Unknown";
		else
			local m = 0;
			for x=1, #Pmobs do
				m = m + Pmobs[x];
			end
			if m < 0 then m = 0; end
			P_M_avg = BarMath_Round(m/#Pmobs,0,BARMATH_ROUNDING_STANDARD);
			P_mobs = BarMath_Round(remaining/P_M_avg,0,BARMATH_ROUNDING_UP);
		end

		-- Determine what text to use if the text length is short or long.
		if (BarMath_GetVar("Bars", "PetXPBar", "TextLength") == BARMATH_TEXT_SHORT) then
			txt = BARMATH_TXT_PXP_SHORT;
		else
			txt = BARMATH_TXT_PXP_LONG;
		end

		-- Set all the options!
		-- Option number must match text number!
		option[1] = txt[1]..BarMath_AddCommas(bars);
		option[2] = txt[2]..BarMath_AddCommas(perbar);
		option[3] = txt[3]..BarMath_AddCommas(currXP).." / "..BarMath_AddCommas(nextXP);
		option[4] = txt[4]..BarMath_AddCommas(XPtogo);
		option[5] = txt[5]..BarMath_AddCommas(togo);
		option[6] = txt[6]..BarMath_AddCommas(pcent).."%";
		option[7] = txt[7]..BarMath_AddCommas(pcent_togo).."%";
		option[8] = txt[8]..BarMath_AddCommas(xpperpcent);
		option[9] = txt[9]..BarMath_AddCommas(xptomax);
		option[10] = txt[10]..BarMath_AddCommas(barstomax);
		option[11] = txt[11]..BarMath_AddCommas(P_mobs);
		option[12] = txt[11]..BarMath_AddCommas(P_mobs)..txt[12]..BarMath_AddCommas(P_M_avg)..BARMATH_BARTXT_PREDICTION_PXP;

		-- Determine which options need to be displayed.
		for i=1, BarMath_DisplayCount, 1 do
			local thisopt = BarMath_GetVar("Bars","PetXPBar","Displays","Display"..i);
			display[i] = tostring(option[thisopt]);
		end

		-- Set the Displays!
		for i=1, BarMath_DisplayCount, 1 do
			BarMath_SetBarText(display[i], "PetXP", i)
		end

		-- Set the Bar!
		BarMathPetXPBar:SetMinMaxValues(min(0, currXP), nextXP)
		BarMathPetXPBar:SetValue(currXP);
		BarMathPetXPBar:SetStatusBarColor(0.58, 0.0, 0.55, 1.0);
		BarMath_Post_Load();

		-- Run Show/Hide to finalize any changes
		BarMath_Bar_ShowHide("PetXP");
	end
end

--== XP Bar Listing Functions ==--

-- Calculates PerBars, Current Bars, Bars To Go, XP To Go, Current Percentage and Needed Percentage.
function BarMath_PetXP_Calculate()
	local perbar, bars, togo, phave, pneed, current, max, max2;

	local current, max = GetPetExperience();

	perbar = BarMath_Round(max/BarMath_NumBars);
	XPTogo = max - current;

	local amount = (current/max)*100;
	phave = BarMath_Round(amount);
	pneed = BarMath_Round(100-phave);
	bars = BarMath_Round(current/perbar);
	togo = BarMath_Round(BarMath_NumBars-bars);

	return perbar, bars, togo, XPTogo, phave, pneed;
end

-- Determines the XP needed to reach the Maximum Level (Currently 70)
function BarMath_PetXPtoMax()
	local result = 0;
	local current = GetPetExperience();
	for lvl = current, BarMath_MaxLVL do
		local thisxp = BarMath_PetXP_Table[lvl];
		if not thisxp then
			thisxp = 0;
		end
		BarMath_Msg("Current Level: "..tostring(lvl)..", XP Amount: "..tostring(thisxp),"debug")
		result = result + thisxp;
	end
	result = result - current;
	return result;
end

function BarMath_PetXP_Prediciton()
	local xpgained = BMPXP_CurrentXP-BMPXP_LastXP;
	local list, vari;

	if (BMPXP_Prediciton == "Exploring") then
		return;
	end

	if (BMPXP_Prediciton == "Mob") then
		list = "Prediction_MobList";
		vari = "PredictionTrackKills";
	end

	local db = BarMath_GetVar("Bars","PetXPBar",list);
	if xpgained ~= 0 then
		tinsert(db,(#db+1),xpgained);
	end
	local ndb = BarMath_PetXP_PredictionTableCleanup(db, vari);
	BarMath_SetVar(ndb,"Bars","PetXPBar",list);
end

function BarMath_PetXP_PredictionTableCleanup(t,v)
	local max = BarMath_GetVar("Bars","PetXPBar",v);
	if t == nil then
		--BarMath_Msg("Returning Blank for "..v);
		return {};
	end
	if max == nil then
		max = 100;
	end
	if (tonumber(#t) <= tonumber(max)) then
		--BarMath_Msg("Returning T for "..v..", Num: "..#t);
		return t;
	end

	local temp1, temp2 = {}, {};

	for x=2,#t do
		if t[x] ~= nil and t[x] > 0 then
			tinsert(temp1, t[x])			
		end
	end

	if #temp1 < max then
		return temp1;
	end

	for y=1, max do
		tinsert(temp2, temp1[y]);
	end

	--BarMath_Msg("Returning Temp for "..v..", Num: "..#temp);

	return temp2;
end

function BarMath_PetXP_Prediciton_Mob()
	BMPXP_Prediciton = "Mob";
end

function BarMath_PetXP_LevelUp(newlevel)
	-- newlevel isn't used, but I put it here in case I DO need it later down the development road.
	local mdb = BarMath_GetVar("Bars","PetXPBar","Prediction_MobList");

	local nmdb = BarMath_PetXP_PredictionTableCleanup(mdb,"PredictionTrackKills");

	BarMath_SetVar(nmdb,"Bars","PetXPBar","Prediction_MobList");

	BarMath_PetXP_Update();
end

--[[ Option Window Data ]]--

-- Option Window information;
function BarMath_PetXP_Options()
	-- Used for listing tabs & their templates. If XP didn't need any tabs yet, we would leave this table empty.
	local tabs = {
		-- Tab Number
		[1] = {
			-- The 2 Tab variables.
			["TabTitle"] = "Prediction",			-- The title of the Tab, as seen in the options panel.
			["TabTemp"] = "BarMath_PetXP_Tab_Prediction",	-- The template to use for this tab.
		},
	};
	local BarTitle = "PetXP"; 	-- Used internally. Doesn't need localization.
	local displayname = BARMATH_PETXP_TITLE; -- The name of the Addon. Used in the Options list.
	BarMath_Generate_Options(BarTitle,displayname,BarMath_Option_DropDown_PetXP_Listing,tabs);
end

-- Load Option Window information!
BarMath_PetXP_Options();

--[[ Function Hooks ]]--
BarMath_AddEvent("PET_UI_UPDATE",BarMath_PetXP_Update);
BarMath_AddEvent("UNIT_PET_EXPERIENCE",BarMath_PetXP_Update);
BarMath_AddEvent("PLAYER_ENTER_COMBAT",BarMath_PetXP_Prediciton_Mob);
BarMath_AddEvent("PLAYER_REGEN_DISABLED",BarMath_PetXP_Prediciton_Mob);
BarMath_AddEvent("UNIT_PET",BarMath_PetXP_Update);
BarMath_AddEvent("PET_STABLE_UPDATE",BarMath_PetXP_Update);

-- Fix Outstanding DB Errors
BarMath_PetXP_LevelUp();

-- Loaded Message
-- Comes last to ensure that everything has been loaded first!
BarMath_HaveLoaded["Addons"]["PetXP"] = BARMATH_PETXP_TITLE.." Addon v"..BarMath_PetXP_Version.." Loaded!";
