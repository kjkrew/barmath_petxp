--[[
	BarMath XP Localization file
	English Edition
	Version 1.0
]]

BARMATH_PETXP_TITLE = "Pet Experience (XP)";


-- XP
BARMATH_DISPLAYOPTION_BAR_FILLED = "Amount of Bars Filled";
BARMATH_DISPLAYOPTION_BAR_PERBAR = "Experience Per Bar";
BARMATH_DISPLAYOPTION_BAR_CURRMAX = "Current / Maximum";
BARMATH_DISPLAYOPTION_BAR_TOLVL = "Needed to Level"; --  (Max - Current)
BARMATH_DISPLAYOPTION_BAR_TOFILL = "Bars needed to Level";
BARMATH_DISPLAYOPTION_PERC_FILLED = "Percentage Filled";
BARMATH_DISPLAYOPTION_PERC_TOFILL = "Percentage Needed to Level";
BARMATH_DISPLAYOPTION_PERC_PERBAR = "Experience Per Percent";
BARMATH_DISPLAYOPTION_RESTED_PXP = "Amount of Rested Experience"
BARMATH_DISPLAYOPTION_PXPTOMAX = "XP Needed to Become Level 80"
BARMATH_DISPLAYOPTION_BARSTOMAX = "Bars Needed to Become Level 80";
BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVEL = "Prediction: Mobs to Level";
BARMATH_DISPLAYOPTION_PREDICTION_QUESTSTOLEVEL = "Prediction: Quests to Level";
BARMATH_DISPLAYOPTION_PREDICTION_MOBSTOLEVELWAVG = "Prediction: Mobs to Level, With Average";
BARMATH_DISPLAYOPTION_PREDICTION_QUESTSTOLEVELWAVG = "Prediction: Quests to Level, With Average";


-- Text that appears on the Bars
-- Long Text
BARMATH_BARTXT_PEXP_BARS_FILLED = "Bars Filled: ";
BARMATH_BARTXT_PEXP_PERBAR = "Experience Per Bar: ";
BARMATH_BARTXT_PEXP = "Pet Experience ";
BARMATH_BARTXT_PEXP_NEEDED_TO_LEVEL = "Experience Needed to Level: ";
BARMATH_BARTXT_PEXP_BARS_TO_LEVEL = "Bars Needed to Level: ";
BARMATH_BARTXT_PEXP_PERCENT_EARNED = "Percent Earned: ";
BARMATH_BARTXT_PEXP_PERCENT_TO_LEVEL = "Percent Needed to Level: ";
BARMATH_BARTXT_PEXP_PER_PERCENT = "Experience Per Percent: ";
BARMATH_BARTXT_PEXP_RESTED = "Rested Experience: ";
BARMATH_BARTXT_PEXP_TO_MAX = "Experience Needed for Level 80: ";
BARMATH_BARTXT_PEXP_BARS_TO_MAX = "Bars Needed for Level 80: ";
BARMATH_BARTXT_PEXP_PREDICTION_MOBSTOLEVEL = "Mobs to Level: ";
BARMATH_BARTXT_PEXP_PREDICTION_QUESTSTOLEVEL = "Quests to Level: ";
BARMATH_BARTXT_PEXP_PREDICTION_AVERAGE = " with an average of: ";

-- Short Text
BARMATH_BARTXT_PXP_BARS_FILLED = "Bars: "
BARMATH_BARTXT_PXP_PERBAR = "XP Per Bar: ";
BARMATH_BARTXT_PXP = "Pet XP ";
BARMATH_BARTXT_PXP_NEEDED_TO_LEVEL = "XP Needed: ";
BARMATH_BARTXT_PXP_BARS_TO_LEVEL = "Bars Needed: ";
BARMATH_BARTXT_PXP_PERCENT_EARNED = "Percent: ";
BARMATH_BARTXT_PXP_PERCENT_TO_LEVEL = "% To Level: "
BARMATH_BARTXT_PXP_PER_PERCENT = "XP Per %: ";
BARMATH_BARTXT_PXP_RESTED = "Rested XP: ";
BARMATH_BARTXT_PXP_TO_MAX = "XP To 80: "
BARMATH_BARTXT_PXP_BARS_TO_MAX = "Bars to 80: ";
BARMATH_BARTXT_PXP_PREDICTION_MOBSTOLEVEL = "Mobs: ";
BARMATH_BARTXT_PXP_PREDICTION_QUESTSTOLEVEL = "Quests: ";
BARMATH_BARTXT_PXP_PREDICTION_AVERAGE = " x ";

BARMATH_BARTXT_PREDICTION_PXP = " XP";

-- Tooltips
BARMATH_PXP_TOOLTIP_TITLE_PREDICTIONQUESTS = "Quest Prediction Number";
BARMATH_PXP_TOOLTIP_TITLE_PREDICTIONMOBS = "Mob Prediction Number";
BARMATH_PXP_TOOLTIP_TEXT_GENERALAVERAGE = "The higher the average number, the\nbetter the prediction system will\nhandle abberations. However, this\nwill also take up more memory.";
BARMATH_PXP_TOOLTIP_TEXT_PREDICTIONQUESTS = "The Number of quests to average.\n\n"..BARMATH_PXP_TOOLTIP_TEXT_GENERALAVERAGE;
BARMATH_PXP_TOOLTIP_TEXT_PREDICTIONMOBS = "The Number of mobs to average.\n\n"..BARMATH_PXP_TOOLTIP_TEXT_GENERALAVERAGE;

BARMATH_NOPETDATA = "No Pet Data";















-- Command-line Options
BARMATH_CMD_PXPTOGO = "xptogo";













-- GUI
BARMATH_GUI_TAB_PEXP = "Experience";
BARMATH_GUI_PERBAR = "XP Per Bar";
BARMATH_GUI_PXPNEEDED = "XP To Level";
BARMATH_GUI_PXP_ORIG = "Current / Max XP";

-- Tooltips
BARMATH_TOOLTIP_CURRENTBARS = "Show or hide how many bars of\nexperience you currently have.";
BARMATH_TOOLTIP_PERBAR = "Show or hide how much XP is in\na single bar.";
BARMATH_TOOLTIP_PXPNEEDED = "Show or hide how much experience\nyou need in order to level.";
BARMATH_TOOLTIP_TOGO = "Show or hide how many bars of\nexperience you need in order\nto level.";
BARMATH_TOOLTIP_PXP_ORIG = "Display Blizzard's original\nXP information.";
BARMATH_TOOLTIP_CURRENTBARS_PERCENT = "Show or hide how much experience\n you currently have, as a percentage.";
BARMATH_TOOLTIP_TOGO_PERCENT = "Show or hide how much experience\nyou need in order to level,\nas a percentage.";
BARMATH_TOOLTIP_TEXTLONG = "Show long text on your\nexperience bar.";
BARMATH_TOOLTIP_TEXTSHORT = "Show short text on your\nexperience bar.";
BARMATH_OPTIONS_TITLE_PXPONOFF = "Show Experience Bar Data";
BARMATH_OPTIONS_TOOLTIP_PXPONOFF = "When clicked, will always show the\ninformation on your experience bar.\n\nIf not clicked, XP info will only show up\nwhen the mouse is over the XP bar.";