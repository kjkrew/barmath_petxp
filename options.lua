--[[
	BarMath Pet XP Options File
	Version 1.0
]]

function BarMath_Options_PetXP_OnLoad()
	BarMath_Options_PetXP_Tooltip_Mobs_Title = BARMATH_XP_TOOLTIP_TITLE_PREDICTIONMOBS;
	BarMath_Options_PetXP_Tooltip_Mobs_Text = BARMATH_XP_TOOLTIP_TEXT_PREDICTIONMOBS;
end